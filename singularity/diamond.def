BootStrap: docker
From: alpine:latest

%post
  apk update && apk add  --no-cache --virtual .build-deps-tmp gcc g++ cmake zlib-dev ninja git
  apk add --no-cache libstdc++ zlib
  GITTAG="master"
  GITURL="https://github.com/bbuchfink/diamond.git"
  BUILDDIR="/builds"
  DST_DIR="dmnd"
  mkdir ${BUILDDIR} && cd ${BUILDDIR} && git clone --branch=${GITTAG} ${GITURL} ${DST_DIR}
  mkdir -p ${BUILDDIR}/${DST_DIR}/build && cd ${BUILDDIR}/${DST_DIR}/build
  cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_BUILD_MARCH=x86-64 ..
  ninja && ninja install
  mkdir /scratch /project

  # Clean up
  apk del .build-deps-tmp
  rm -rf ${BUILDDIR}

%runscript
  exec /usr/local/bin/diamond "$@"

%help
  Run diamond [0] in Singularity
  Usage:
    singularity run diamond_container -h
    singularity run diamond_container makedb --db dbname --in sequences.fasta
  [0]:Buchfink B, Xie C, Huson DH, "Fast and sensitive protein alignment using DIAMOND", Nature Methods 12, 59-60 (2015). doi:10.1038/nmeth.3176

%labels
  Diamond "Buchfink B, Xie C, Huson DH, "Fast and sensitive protein alignment using DIAMOND", Nature Methods 12, 59-60 (2015). doi:10.1038/nmeth.3176"
  Container jpb@members.fsf.org
